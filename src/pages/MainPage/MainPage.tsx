import InfoFrame from '../../components/InfoFrame/InfoFrame';
import SpeakersFrame from '../../components/SpeakersFrame/SpeakersFrame';

import styles from './MainPage.module.scss';

const MainPage = () => {
  return (
    <div className={styles.mainPage}>
      <InfoFrame />
      <SpeakersFrame />
    </div>
  );
};

export default MainPage;
