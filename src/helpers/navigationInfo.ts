import vk from '../assets/logo/vk.svg';
import facebook from '../assets/logo/facebook.svg';
import instagram from '../assets/logo/instagram.svg';
import telegram from '../assets/logo/telegram.svg';

export const headerNavigationLink = [
  { text: 'О мероприятии', url: '#information' },
  { text: 'Спикеры', url: '#speakers' }
];

export const additionalInfo = [
  { text: 'Политика конфиденциальности', url: '' },
  { text: 'Публичная оферта', url: '' },
  { text: '+7 912 123-45-67', url: 'tel:+79121234567' },
  { text: 'about@microsoftforum.com', url: 'mailto:about@microsoftforum.com' }
];
export const navigationsLink = [
  { text: 'О мероприятии', url: '#information' },
  { text: 'Темы', url: '' },
  { text: 'Расписание', url: '' },
  { text: 'Спикеры', url: '#speakers' },
  { text: 'Контакты', url: '' }
];
export const socialMedia = [
  { icon: vk, url: 'http://vk.com' },
  { icon: facebook, url: 'http://facebook.com' },
  { icon: instagram, url: 'http://instagram.com' },
  { icon: telegram, url: 'http://t.me' }
];
