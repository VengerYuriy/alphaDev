import { FC } from 'react';
import styles from './SpeakerCard.module.scss';
import ISpeakerItem from '../../types/ISpeakerItem';

export const SpeakerCard: FC<ISpeakerItem> = (props) => {
  return (
    <article className={styles.speakersCard}>
      <img src={props.photo_url} alt={`${props.name} photo`} />
      <h3>{props.name}</h3>
      <hr />
      <div className={styles.speakersCard__descriptionWrapper}>
        <p>{props.description}</p>
        <a>Подробнее</a>
      </div>
    </article>
  );
};
