import { FC } from 'react';
import styles from './SocialLogo.module.scss';

interface ISocialLogoProps {
  icon: string;
  url: string;
}

export const SocialLogo: FC<ISocialLogoProps> = ({ icon, url }) => (
  <a href={url} target="_blank" className={styles.socialLogo}>
    <img src={icon} className={styles.socialLogo__icon} />
  </a>
);
