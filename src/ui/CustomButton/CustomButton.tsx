import styles from './CustomButton.module.scss';

interface ICustomButtonProps {
  value: string;
}

export const CustomButton: React.FunctionComponent<ICustomButtonProps> = ({ value }) => {
  return <button className={styles.customButton}>{value}</button>;
};
