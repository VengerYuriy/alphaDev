import styles from './SecurityLogo.module.scss';
import securityImg from '../../assets/logo/security.svg';

export const SecurityLogo = () => (
  <article className={styles.securityLogo}>
    <img className={styles.secrityLogo__image} src={securityImg} />
    <section className={styles.securityLogo__description}>
      <big>Security</big>
      <p>Forum</p>
    </section>
  </article>
);
