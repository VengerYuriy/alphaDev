import axios from 'axios';

const getData = (url: string) => {
  return axios(url)
    .then((resp) => resp.data)
    .catch((err) => console.log(err));
};

export default getData;
