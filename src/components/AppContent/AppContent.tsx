import MainPage from '../../pages/MainPage/MainPage';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';

import styles from './AppContent.module.scss';

const AppContent = () => {
  return (
    <div className={styles.app_content}>
      <Header />
      <MainPage />
      <Footer />
    </div>
  );
};

export default AppContent;
