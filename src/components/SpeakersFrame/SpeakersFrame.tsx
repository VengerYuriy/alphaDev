import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { loadSpeakersAction } from '../../store/speakersStore/actions';
import { AppDispatch } from '../../store/speakersStore/types';
import { selectSpeakers } from '../../store/speakersStore/selectors';

import { speakersUrl } from '../../helpers/requestsUrl';
import { SpeakerCard } from '../../ui';

import styles from './SpeakersFrame.module.scss';

const SpeakersFrame = () => {
  const dispatch: AppDispatch = useDispatch();

  const speakers = useSelector(selectSpeakers);

  useEffect(() => {
    dispatch(loadSpeakersAction(speakersUrl));
  }, []);

  return (
    <section className={styles.speakersFrame} id="speakers">
      <h2>Спикеры нашего форума</h2>
      <section className={styles.speakersList}>
        {speakers.map((speaker) => (
          <SpeakerCard {...speaker} key={speaker.photo_url} />
        ))}
      </section>
    </section>
  );
};

export default SpeakersFrame;
