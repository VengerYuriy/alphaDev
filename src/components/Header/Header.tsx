import { Link, useLocation } from 'react-router-dom';
import { CustomButton, SecurityLogo } from '../../ui';
import styles from './Header.module.scss';
import { headerNavigationLink } from '../../helpers/navigationInfo';
import { useEffect } from 'react';

const Header = () => {
  const { hash } = useLocation();

  useEffect(() => {
    if (hash) {
      const elem = document.getElementById(hash.slice(1));
      if (elem) {
        elem.scrollIntoView({ behavior: 'smooth' });
      }
    } else {
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
    }
  }, [hash]);

  return (
    <header className={styles.header}>
      <SecurityLogo />
      <nav>
        {headerNavigationLink.map((link) => (
          <Link to={link.url} className={hash === link.url ? 'active' : 'non-active'} key={link.url}>
            {link.text}
          </Link>
        ))}
      </nav>
      <CustomButton value="Регистрация" />
    </header>
  );
};

export default Header;
