import styles from './InfoFrame.module.scss';
import { CustomButton } from '../../ui';

const InfoFrame = () => {
  return (
    <section className={styles.infoFrame} id="information">
      <h1>Форум Microsoft</h1>
      <h1>«Finance industry trust in AI and IT-Security»</h1>
      <h3>
        Масштабная кибератака и устаревшее оборудование может привести к потере клиентов, ухудшить отношения с
        партнёрами и учредителями, а невыполнение требований законодательства может привести к существенным штрафам и
        даже к блокировке ресурсов
      </h3>
      <p>Узнайте как этого избежать, регистрируйтесь на наш форум</p>
      <CustomButton value="Регистрация" />
    </section>
  );
};

export default InfoFrame;
