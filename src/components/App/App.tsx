import { Provider } from 'react-redux';
import AppContent from '../AppContent/AppContent';
import store from '../../store';
import { BrowserRouter } from 'react-router-dom';

const App = () => {
  return (
    <BrowserRouter>
      <Provider store={store}>
        <AppContent />
      </Provider>
    </BrowserRouter>
  );
};

export default App;
