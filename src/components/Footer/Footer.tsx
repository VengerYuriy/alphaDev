import { SecurityLogo, SocialLogo } from '../../ui';
import { additionalInfo, navigationsLink, socialMedia } from '../../helpers/navigationInfo';

import styles from './Footer.module.scss';
import { Link } from 'react-router-dom';

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <div className={styles.footer__securityLogo}>
        <SecurityLogo />
      </div>
      <div className={styles.footerColumn}>
        {additionalInfo.map((info, index) => (
          <a href={info.url} className={styles.footerColumn__additionalInfo} key={index}>
            {info.text}
          </a>
        ))}
      </div>
      <nav className={styles.footerColumn}>
        {navigationsLink.map((navItem, index) => (
          <Link to={navItem.url} key={index}>
            {navItem.text}
          </Link>
        ))}
      </nav>
      <section className={styles.footerColumn_contacts}>
        {socialMedia.map((network, index) => (
          <SocialLogo {...network} key={index} />
        ))}
      </section>
    </footer>
  );
};

export default Footer;
