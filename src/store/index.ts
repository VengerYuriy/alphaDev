import { applyMiddleware, combineReducers, legacy_createStore as createStore } from 'redux';
import { ISpeakersReducer } from './speakersStore/types';
import speakersReducer from './speakersStore/reducer';
import thunk from 'redux-thunk';

export interface IStore {
  speakersReducer: ISpeakersReducer;
}

const rootReducer = combineReducers({ speakersReducer });
const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
