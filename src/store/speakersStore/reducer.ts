import { ISpeakersAction } from './types';

const initialState = {
  speakers: []
};

const speakersReducer = (state = initialState, action: ISpeakersAction) => {
  switch (action.type) {
    case 'people/setSpeakers':
      return { ...state, speakers: action.payload.response };
    default:
      return state;
  }
};

export default speakersReducer;
