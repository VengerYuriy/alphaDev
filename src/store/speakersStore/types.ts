import { Dispatch } from 'react';
import ISpeakerItem from '../../types/ISpeakerItem';

export type AppDispatch = Dispatch<any>;

export interface ISpeakersResponce {
  response: ISpeakerItem[];
  success: boolean;
}

export interface ISpeakersAction {
  type: string;
  payload: ISpeakersResponce;
}

export interface ISpeakersReducer {
  speakers: ISpeakerItem[];
}
