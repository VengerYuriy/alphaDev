import { IStore } from '..';

export const selectSpeakers = (state: IStore) => state.speakersReducer.speakers;
