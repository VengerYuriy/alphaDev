import { Dispatch } from 'redux';
import getData from '../../services/getData';
import { ISpeakersAction, ISpeakersResponce } from './types';

export const setSpeakersAction = (data: ISpeakersResponce) => {
  return {
    type: 'people/setSpeakers',
    payload: data
  };
};

export const loadSpeakersAction = (url: string) => async (dispatch: Dispatch<ISpeakersAction>) => {
  try {
    const data = await getData(url);
    dispatch(setSpeakersAction(data));
  } catch (e) {
    console.log(e);
  }
};
