export default interface ISpeakerItem {
  description: string;
  name: string;
  performance_time: string;
  photo_url: string;
}
